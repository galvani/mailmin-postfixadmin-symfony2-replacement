<?php

namespace Galvani\Bundle\MailminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Forwarding
 *
 * @ORM\Table("forwardings")
 * @ORM\Entity
 */
class Forwarding
{
    /**
     * @var string
	 * @ORM\Id
     *
     * @ORM\Column(name="source", type="string", length=80)
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(name="destination", type="text")
     */
    private $destination;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set source
     *
     * @param string $source
     * @return Forwarding
     */
    public function setSource($source)
    {
        $this->source = $source;
    
        return $this;
    }

    /**
     * Get source
     *
     * @return string 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set destination
     *
     * @param string $destination
     * @return Forwarding
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    
        return $this;
    }

    /**
     * Get destination
     *
     * @return string 
     */
    public function getDestination()
    {
        return $this->destination;
    }
}
