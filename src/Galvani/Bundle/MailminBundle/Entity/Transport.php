<?php

namespace Galvani\Bundle\MailminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transport
 *
 * @ORM\Table("transport")
 * @ORM\Entity
 */
class Transport
{
    /**
     * @var string
	 * @ORM\Id
     * @ORM\Column(name="domain", type="string", length=50)
     */
    private $domain;

    /**
     * @var string
     *
     * @ORM\Column(name="transport", type="string", length=128)
     */
    private $transport;

    /**
     * Set domain
     *
     * @param string $domain
     * @return Transport
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    
        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set transport
     *
     * @param string $transport
     * @return Transport
     */
    public function setTransport($transport)
    {
        $this->transport = $transport;
    
        return $this;
    }

    /**
     * Get transport
     *
     * @return string 
     */
    public function getTransport()
    {
        return $this->transport;
    }
}
